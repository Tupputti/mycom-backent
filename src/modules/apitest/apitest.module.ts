import { Module } from '@nestjs/common';
import { ApitestService } from './apitest.service';
import { ApitestController } from './apitest.controller'


@Module({
  providers: [ApitestService],
  controllers: [ApitestController]
})
export class ApitestModule {}
