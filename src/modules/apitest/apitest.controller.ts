import { Controller, Get } from '@nestjs/common';
import { ApitestService } from './apitest.service'



@Controller('apitest')
export class ApitestController {
    constructor(private apitestService:ApitestService){}

  @Get()
  getHello(): string {
    return this.apitestService.getHello();
    
  }
}
