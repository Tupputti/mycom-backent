import { Entity, Column, PrimaryGeneratedColumn, OneToMany} from 'typeorm';
import { Computers } from '../computers/computers.entity';

@Entity()
export class Mainboards {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  mainboardName: string;

  @Column()
  mainboardDetail: string;

  @Column()
  mainboardPrice: number;

  @OneToMany(type => Computers, computer => computer.id)
  computer: Computers[];

}