import { Controller, Body, Post, Get, Param, Delete, Put } from '@nestjs/common';
import { MainboardsService } from './mainboards.service';
import { Mainboards } from './mainboards.entity';

@Controller('mainboards')
export class MainboardsController {
    constructor(private mainboardsService:MainboardsService){

    }
    @Get()
    async findAllMainboard(){
        return await this.mainboardsService.findAll()
    }

    @Get(':id')
    async findMainboard(@Param('id') id: string){
        return await this.mainboardsService.findOne(id)
    }


    @Post()
    async createMainboard(@Body() payload:Mainboards): Promise<Mainboards>{
         return await this.mainboardsService.createMainboard(payload);
    }

    @Delete(':id')
    async deleteMainboard(@Param('id') id: string){
        await this.mainboardsService.removeMainboard(id)
    }
    
    @Put(':id/update')
    async update(@Param('id') id, @Body() mainboard: Mainboards): Promise<any> {
        mainboard.id = Number(id);
        return this.mainboardsService.updateMainboard(mainboard);
    }  
}
