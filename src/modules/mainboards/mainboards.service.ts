import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Mainboards } from './mainboards.entity';

@Injectable()
export class MainboardsService {
    constructor(
        @InjectRepository(Mainboards)
        private mainboardsRepository: Repository<Mainboards>){
        }

        findAll(): Promise<Mainboards[]> {
            return this.mainboardsRepository.find();
          }

        findOne(id: string): Promise<Mainboards> {
            return this.mainboardsRepository.findOne(id);
        }

        createMainboard(mainboard:Mainboards){
            return this.mainboardsRepository.save(mainboard);
        }

        removeMainboard(id: string){
            this.mainboardsRepository.delete(id);
        }

        updateMainboard(mainboard:Mainboards){
            this.mainboardsRepository.update(mainboard.id, mainboard);
        }
}
