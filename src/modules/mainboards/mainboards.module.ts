import { Module } from '@nestjs/common';
import { MainboardsController } from './mainboards.controller';
import { MainboardsService } from './mainboards.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Mainboards } from './mainboards.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Mainboards]),
    ],
  controllers: [MainboardsController],
  providers: [MainboardsService]
})
export class MainboardsModule {}
