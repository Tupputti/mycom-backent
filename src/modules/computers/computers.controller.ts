import { Controller, Post, Body, Get, Delete, Put, Param } from '@nestjs/common';
import { ComputersService } from './computers.service';
import { Computers } from './computers.entity';

@Controller('computers')
export class ComputersController {
    constructor(private computersService:ComputersService){

    }
    @Get()
    async findAllComputer(){
        return await this.computersService.findAll()
    }

    @Get(':id')
    async findComputer(@Param('id') id: string){
        return await this.computersService.findOne(id)
    }


    @Post()
    async createComputer(@Body() payload:Computers): Promise<Computers>{
         return await this.computersService.createComputer(payload);
    }

    @Delete(':id')
    async deleteComputer(@Param('id') id: string){
        await this.computersService.removeComputer(id)
    }
    
    @Put(':id/update')
    async update(@Param('id') id, @Body() computer: Computers): Promise<any> {
        computer.id = Number(id);
        return this.computersService.updateComputer(computer);
    }  
}
