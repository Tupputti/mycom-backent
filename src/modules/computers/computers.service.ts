import { Injectable } from '@nestjs/common';
import { Computers } from './computers.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ComputersService {
    constructor(
        @InjectRepository(Computers)
        private computersRepository: Repository<Computers>,){
        }

        findAll(): Promise<Computers[]> {
            return this.computersRepository.find();
          }

        findOne(id: string): Promise<Computers> {
            return this.computersRepository.findOne(id);
        }

        createComputer(computer:Computers){
            return this.computersRepository.save(computer);
        }

        removeComputer(id: string){
            this.computersRepository.delete(id);
        }

        updateComputer(computer:Computers){
            this.computersRepository.update(computer.id, computer);
        }
}
