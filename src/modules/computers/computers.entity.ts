import { Entity, Column, PrimaryGeneratedColumn, ManyToOne} from 'typeorm';
import { Cpus } from '../cpus/cpus.entity';
import { Graphics } from '../graphics/graphics.entity';
import { Mainboards } from '../mainboards/mainboards.entity';
import { Rams } from '../rams/rams.entity';

@Entity()
export class Computers {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  computerName: string;

  @Column()
  computerPrice: number;

  @ManyToOne(type => Cpus, cpu => cpu.id)
  cpu: Cpus[];

  @ManyToOne(type => Graphics, graphic => graphic.id)
  graphic: Graphics[];

  @ManyToOne(type => Mainboards, mainboard => mainboard.id)
  mainboard: Mainboards[];

  @ManyToOne(type => Rams, ram => ram.id)
  ram: Rams[];
}