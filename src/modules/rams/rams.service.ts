import { Injectable } from '@nestjs/common';
import { Rams } from './rams.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class RamsService {
    constructor(
        @InjectRepository(Rams)
        private ramsRepository: Repository<Rams>){
        }

        findAll(): Promise<Rams[]> {
            return this.ramsRepository.find();
          }

        findOne(id: string): Promise<Rams> {
            return this.ramsRepository.findOne(id);
        }

        createRam(ram:Rams){
            return this.ramsRepository.save(ram);
        }

        removeRam(id: string){
            this.ramsRepository.delete(id);
        }

        updateRam(ram:Rams){
            this.ramsRepository.update(ram.id, ram);
        }
}
