import { Controller, Body, Post, Get, Param, Delete, Put } from '@nestjs/common';
import { RamsService } from './rams.service';
import { Rams } from './rams.entity';

@Controller('rams')
export class RamsController {
    constructor(private ramsService:RamsService){

    }
    @Get()
    async findAllRam(){
        return await this.ramsService.findAll()
    }

    @Get(':id')
    async findRam(@Param('id') id: string){
        return await this.ramsService.findOne(id)
    }


    @Post()
    async createRam(@Body() payload:Rams): Promise<Rams>{
         return await this.ramsService.createRam(payload);
    }

    @Delete(':id')
    async deleteRam(@Param('id') id: string){
        await this.ramsService.removeRam(id)
    }
    
    @Put(':id/update')
    async update(@Param('id') id, @Body() ram: Rams): Promise<any> {
        ram.id = Number(id);
        return this.ramsService.updateRam(ram);
    }  
}
