import { Entity, Column, PrimaryGeneratedColumn, OneToMany} from 'typeorm';
import { Computers } from '../computers/computers.entity';

@Entity()
export class Rams {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  ramName: string;

  @Column()
  ramDetail: string;

  @Column()
  ramPrice: number;

  @OneToMany(type => Computers, computer => computer.id)
  computer: Computers[];

}