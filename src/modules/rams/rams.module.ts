import { Module } from '@nestjs/common';
import { RamsController } from './rams.controller';
import { RamsService } from './rams.service';
import { Rams } from './rams.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forFeature([Rams]),
    ],
  controllers: [RamsController],
  providers: [RamsService]
})
export class RamsModule {}
