import { Controller, Body, Post, Get, Delete, Param, Put } from '@nestjs/common';
import { Cpus } from './cpus.entity';
import { CpusService } from './cpus.service';

@Controller('cpus')
export class CpusController {

    constructor(private cpusService:CpusService){

    }

    @Get()
    async findAllCpu(){
        return await this.cpusService.findAll()
    }

    @Get(':id')
    async findCpu(@Param('id') id: string){
        return await this.cpusService.findOne(id)
    }


    @Post()
    async createCpu(@Body() payload:Cpus): Promise<Cpus>{
         return await this.cpusService.createCpu(payload);
    }

    @Delete(':id')
    async deleteCpu(@Param('id') id: string){
        await this.cpusService.removeCpu(id)
    }
    
    @Put(':id/update')
    async update(@Param('id') id, @Body() cpu: Cpus): Promise<any> {
        cpu.id = Number(id);
        return this.cpusService.updateCpu(cpu);
    }  
}
