import { Module } from '@nestjs/common';
import { CpusController } from './cpus.controller';
import { CpusService } from './cpus.service';
import { Cpus } from './cpus.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forFeature([Cpus]),
    ],
  controllers: [CpusController],
  providers: [CpusService]
})
export class CpusModule {}
