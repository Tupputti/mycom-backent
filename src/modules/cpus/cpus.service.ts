import { Injectable} from '@nestjs/common';
import { Cpus } from './cpus.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class CpusService {
    constructor(
        @InjectRepository(Cpus)
        private cpusRepository: Repository<Cpus>,){
        }

        findAll(): Promise<Cpus[]> {
            return this.cpusRepository.find();
          }

        findOne(id: string): Promise<Cpus> {
            return this.cpusRepository.findOne(id);
        }

        createCpu(cpu:Cpus){
            return this.cpusRepository.save(cpu);
        }

        removeCpu(id: string){
            this.cpusRepository.delete(id);
        }

        updateCpu(cpu:Cpus){
            this.cpusRepository.update(cpu.id, cpu);
        }
    }  

