import { Entity, Column, PrimaryGeneratedColumn, OneToMany} from 'typeorm';
import { Computers } from '../computers/computers.entity';

@Entity()
export class Cpus {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  cpuName: string;

  @Column()
  cpuDetail: string;

  @Column()
  cpuPrice: number;

  @OneToMany(type => Computers, computer => computer.id)
  computer: Computers[];

}