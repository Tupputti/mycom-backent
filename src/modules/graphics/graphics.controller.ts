import { Controller, Post, Body, Get, Param, Delete, Put } from '@nestjs/common';
import { Graphics } from './graphics.entity';
import { GraphicsService } from './graphics.service';

@Controller('graphics')
export class GraphicsController {
    constructor(private graphicsService:GraphicsService){

    }
    @Get()
    async findAllGraphic(){
        return await this.graphicsService.findAll()
    }

    @Get(':id')
    async findGraphic(@Param('id') id: string){
        return await this.graphicsService.findOne(id)
    }


    @Post()
    async createGraphic(@Body() payload:Graphics): Promise<Graphics>{
         return await this.graphicsService.createGraphic(payload);
    }

    @Delete(':id')
    async deleteGraphic(@Param('id') id: string){
        await this.graphicsService.removeGraphic(id)
    }
    
    @Put(':id/update')
    async update(@Param('id') id, @Body() Graphic: Graphics): Promise<any> {
        Graphic.id = Number(id);
        return this.graphicsService.updateGraphic(Graphic);
    }  
}
