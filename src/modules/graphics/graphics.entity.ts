import { Entity, Column, PrimaryGeneratedColumn, OneToMany} from 'typeorm';
import { Computers } from '../computers/computers.entity';

@Entity()
export class Graphics {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  graphicName: string;

  @Column()
  graphicDetail: string;

  @Column()
  graphicPrice: number;

  @OneToMany(type => Computers, computer => computer.id)
  computer: Computers[];

}