import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Graphics } from './graphics.entity';

@Injectable()
export class GraphicsService {
    constructor(
        @InjectRepository(Graphics)
        private graphicsRepository: Repository<Graphics>){
        }

        findAll(): Promise<Graphics[]> {
            return this.graphicsRepository.find();
          }

        findOne(id: string): Promise<Graphics> {
            return this.graphicsRepository.findOne(id);
        }

        createGraphic(graphic:Graphics){
            return this.graphicsRepository.save(graphic);
        }

        removeGraphic(id: string){
            this.graphicsRepository.delete(id);
        }

        updateGraphic(graphic:Graphics){
            this.graphicsRepository.update(graphic.id, graphic);
        }
}
