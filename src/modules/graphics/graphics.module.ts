import { Module } from '@nestjs/common';
import { GraphicsController } from './graphics.controller';
import { GraphicsService } from './graphics.service';
import { Graphics } from './graphics.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forFeature([Graphics]),
    ],
  controllers: [GraphicsController],
  providers: [GraphicsService]
})
export class GraphicsModule {}
