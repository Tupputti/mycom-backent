import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RamsModule } from './modules/rams/rams.module';
import { CpusModule } from './modules/cpus/cpus.module';
import { ComputersModule } from './modules/computers/computers.module';
import { GraphicsModule } from './modules/graphics/graphics.module';
import { MainboardsModule } from './modules/mainboards/mainboards.module';
import { ApitestModule } from './modules/apitest/apitest.module';


@Module({
  imports: [
  TypeOrmModule.forRoot(),
  RamsModule,
  CpusModule,
  ComputersModule,
  GraphicsModule,
  MainboardsModule,
  ApitestModule, 
  ],
  controllers:[AppController],
  providers:[AppService],
})
export class AppModule {}